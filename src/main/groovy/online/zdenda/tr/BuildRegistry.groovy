package online.zdenda.tr

import online.zdenda.tr.output.HtmlBuilder
import online.zdenda.tr.provider.ConfluenceXmlSpaceParser
import online.zdenda.tr.provider.ConfluenceZipProvider
import online.zdenda.tr.provider.JsonFileProvider
import org.yaml.snakeyaml.Yaml

/**
 * Builds the whole registry in 2 steps.
 * 1. Gets data via data provider
 * 2. Builds output via output builder
 */
class BuildRegistry {

    static ENCODING = "UTF-8"
    static TMP_DIR = new File("./tmp")

    private static DATA_PROVIDERS = [
            "json-file"     : new JsonFileProvider(),
            "confluence-zip": new ConfluenceZipProvider(new ConfluenceXmlSpaceParser())
    ]
    private static OUTPUT_BUILDERS = [
            "html": new HtmlBuilder()
    ]

    static void main(String[] args) {
        def cfg = loadConfiguration(args)
        validateConfiguration(cfg)
        def dataProvider = DATA_PROVIDERS[cfg.data.provider]
        def outputBuilder = OUTPUT_BUILDERS[cfg.output.builder]

        prepareTmpDir()
        def data = dataProvider.provide(cfg)
        outputBuilder.build(cfg, data)
        deleteTmpDir()

        println("==> Registry build successful")
    }

    static exit(String errorMessage) {
        println("!!! $errorMessage")
        System.exit(666)
    }

    private static Configuration loadConfiguration(String[] args) {
        if (args.length != 1) {
            exit("Expecting only single argument pointing to configuration YAML file but got [${String.join(",", args)}]")
        }

        File cfgFile = new File(args[0])
        if (!cfgFile.exists()) {
            exit("Configuration file $cfgFile does not exist")
        }
        println("- Using configuration file $cfgFile.canonicalPath")
        return new Yaml().load(cfgFile.getText(ENCODING))
    }

    private static void validateConfiguration(Configuration cfg) {
        boolean isValid = true

        if (cfg.data == null) {
            println("!!! Data (data) are missing in configuration")
            isValid = false
        } else {
            if (cfg.data.provider == null) {
                println("!!! Data provider (data.provider) is missing in configuration")
                isValid = false
            } else {
                if (DATA_PROVIDERS[cfg.data.provider] == null) {
                    String validProviders = String.join(",", DATA_PROVIDERS.keySet())
                    println("!!! Data provider (data.provider) has invalid value $cfg.data.provider, must be one of [$validProviders]")
                    isValid = false
                }
            }
        }

        if (cfg.output == null) {
            println("!!! Output (output) is missing in configuration")
            isValid = false
        } else {
            if (cfg.output.builder == null) {
                println("!!! Output builder (data.builder) is missing in configuration")
                isValid = false
            } else {
                if (OUTPUT_BUILDERS[cfg.output.builder] == null) {
                    String validOutputs = String.join(",", OUTPUT_BUILDERS.keySet())
                    println("!!! Output builder (output.builder) has invalid value $cfg.output.builder, must be one of [$validOutputs]")
                    isValid = false
                }
            }
        }

        if (!isValid) {
            System.exit(666)
        }
    }

    private static prepareTmpDir() {
        println("- Preparing temporary working directory")
        TMP_DIR.deleteDir()
        TMP_DIR.mkdirs()
    }

    private static deleteTmpDir() {
        println("- Deleting temporary working directory")
        TMP_DIR.deleteDir()
    }
}
