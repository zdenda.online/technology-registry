package online.zdenda.tr.provider

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import online.zdenda.tr.Configuration
import online.zdenda.tr.Data

import static online.zdenda.tr.BuildRegistry.exit

/**
 * Simples provider that loads data from JSON file.
 */
class JsonFileProvider implements DataProvider {

    private final objectMapper = new ObjectMapper()

    @Override
    Data provide(Configuration cfg) {
        File jsonFile = new File(cfg.data.filePath)
        if (!jsonFile.exists()) {
            exit("Path to JSON file $jsonFile.canonicalPath is not an existing file")
        }
        try {
            return objectMapper.readValue(jsonFile, Data.class)
        } catch (JsonProcessingException ex) {
            exit("JSON file $jsonFile.canonicalPath has invalid data structure: $ex.message")
            return null // make compiler happy
        }
    }
}
