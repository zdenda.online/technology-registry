package online.zdenda.tr.provider

import online.zdenda.tr.Data
import online.zdenda.tr.Configuration

/**
 * Parses specific structure of Confluence pages.
 */
interface ConfluenceSpaceParser {

    Data parse(Configuration cfg, String spaceContents)
}