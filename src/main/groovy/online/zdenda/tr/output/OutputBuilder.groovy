package online.zdenda.tr.output

import online.zdenda.tr.Data
import online.zdenda.tr.Configuration

/**
 * Builder of final registry visualization.
 */
interface OutputBuilder {

    /**
     * Builds the final visualization according to configuration.
     *
     * @param cfg configuration
     * @param data data for visualization
     */
    void build(Configuration cfg, Data data)
}
