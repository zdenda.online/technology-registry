package online.zdenda.tr.output

import groovy.json.JsonOutput
import online.zdenda.tr.BuildRegistry
import online.zdenda.tr.Configuration
import online.zdenda.tr.Data

import static online.zdenda.tr.BuildRegistry.exit

/**
 * Builds HTML output in two forms: self-contained HTML file and HTML snippet for embedding.
 */
class HtmlBuilder implements OutputBuilder {

    private static TITLE_PLACEHOLDER = "//TITLE//"
    private static JS_PLACEHOLDER = "//JS//"
    private static HEAD_PLACEHOLDER = "//HEAD//"
    private static CFG_PLACEHOLDER = "//CFG//"
    private static DATA_PLACEHOLDER = "//DATA//"
    private static DESCRIPTION_PLACEHOLDER = "//DESCRIPTION//"
    private static README_JS_FILES_PLACEHOLDER = "//JS_FILES//"

    private static SELF_CONTAINED_TEMPLATE_RESOURCE = "self-contained-template.html"
    private static SNIPPET_TEMPLATE_RESOURCE = "snippet-template.html"
    private static SNIPPET_README_RESOURCE = "snippet-readme.txt"
    private static JS_RESOURCES = ["registry.js", "d3.v4.min.js"]

    private static OUTPUT_SELF_CONTAINED_FILENAME = "registry.html"
    private static OUTPUT_SNIPPET_DIRNAME = "snippet"
    private static OUTPUT_SNIPPET_HTML_FILENAME = "registry.html"
    private static OUTPUT_SNIPPET_README_FILENAME = "readme.txt"
    private static OUTPUT_HEAD_HTML_FILENAME = "head.html"

    void build(Configuration cfg, Data data) {
        def ctx = new BuilderContext(
                cfg: cfg,
                data: data,
                outputDir: new File(cfg.output.dir),
                snippetDir: new File(cfg.output.dir, OUTPUT_SNIPPET_DIRNAME),
                dataJson: JsonOutput.prettyPrint(JsonOutput.toJson(data)),
                configJson: readCustomFile(cfg.output.svgConfigPath),
                headHtml: readCustomFile(cfg.output.headSnippetPath),
                descriptionHtml: readCustomFile(cfg.output.descriptionSnippetPath, false)
        )

        println("- Building HTML registry output to $ctx.outputDir.canonicalPath")
        prepareDirs(ctx)
        processJavaScriptResources(ctx)
        buildSelfContainedHtml(ctx)
        buildSnippetHtmls(ctx)
        copySnippetReadme(ctx)
        println("  - Successfully built HTMLs")
    }

    private String processJavaScriptResources(BuilderContext ctx) {
        println("  - Processing JavaScript resources")
        def selfContainedJs = ""
        JS_RESOURCES.each { jsResource ->
            def jsFileText = readResource(jsResource)

            selfContainedJs += "// Contents of $jsResource\n"
            selfContainedJs += jsFileText
            selfContainedJs += "\n"

            new File(ctx.snippetDir, jsResource).write(jsFileText, BuildRegistry.ENCODING)
        }
        ctx.selfContainedJavaScript = selfContainedJs
    }

    private prepareDirs(BuilderContext ctx) {
        ctx.outputDir.mkdirs()
        ctx.snippetDir.mkdirs()
    }

    private buildSelfContainedHtml(BuilderContext ctx) {
        println("  - Building self-contained HTML file")

        def html = readResource(SELF_CONTAINED_TEMPLATE_RESOURCE)
        html = html.replace(TITLE_PLACEHOLDER, ctx.data.title)
        html = html.replace(JS_PLACEHOLDER, ctx.selfContainedJavaScript)
        html = html.replace(HEAD_PLACEHOLDER, ctx.headHtml)
        html = html.replace(CFG_PLACEHOLDER, ctx.configJson)
        html = html.replace(DATA_PLACEHOLDER, ctx.dataJson)
        html = html.replace(DESCRIPTION_PLACEHOLDER, ctx.descriptionHtml)

        def selfContainedFile = new File(ctx.outputDir, OUTPUT_SELF_CONTAINED_FILENAME)
        selfContainedFile.parentFile.mkdirs()
        selfContainedFile.delete()
        selfContainedFile.write(html, BuildRegistry.ENCODING)
    }

    private buildSnippetHtmls(BuilderContext ctx) {
        println("  - Building snippet HTML files")

        def registryHtml = readResource(SNIPPET_TEMPLATE_RESOURCE)
        registryHtml = registryHtml.replace(CFG_PLACEHOLDER, ctx.configJson)
        registryHtml = registryHtml.replace(DATA_PLACEHOLDER, ctx.dataJson)
        registryHtml = registryHtml.replace(DESCRIPTION_PLACEHOLDER, ctx.descriptionHtml)

        def registryHtmlFile = new File(ctx.snippetDir, OUTPUT_SNIPPET_HTML_FILENAME)
        registryHtmlFile.delete()
        registryHtmlFile.write(registryHtml, BuildRegistry.ENCODING)

        def headHtml = ctx.headHtml
        JS_RESOURCES.each { jsResource ->
            headHtml += "\r\n<script type=\"text/javascript\" src=\"./$jsResource\"></script>"
        }

        def headHtmlFile = new File(ctx.snippetDir, OUTPUT_HEAD_HTML_FILENAME)
        headHtmlFile.delete()
        headHtmlFile.write(headHtml, BuildRegistry.ENCODING)
    }

    private copySnippetReadme(BuilderContext ctx) {
        def readmeText = readResource(SNIPPET_README_RESOURCE)
        readmeText = readmeText.replace(README_JS_FILES_PLACEHOLDER, String.join(", ", JS_RESOURCES))
        def outputReadme = new File(ctx.snippetDir, OUTPUT_SNIPPET_README_FILENAME)
        outputReadme.write(readmeText, BuildRegistry.ENCODING)
    }

    private String readResource(String path) {
        return HtmlBuilder.class.getResourceAsStream("/html/$path").withCloseable {
            it.getText(BuildRegistry.ENCODING)
        }
    }

    private String readCustomFile(String path, boolean isRequired = true) {
        if (path == null || path.trim().isEmpty()) {
            if (isRequired) {
                exit("Path $path is empty but it is required")
            } else {
                return ""
            }
        }
        def file = new File(path)
        if (!file.exists()) {
            exit("Path to file $file.path is not an existing file")
        }
        return file.getText(BuildRegistry.ENCODING)
    }

    // Easier passing of parameters during parsing
    private static class BuilderContext {
        Configuration cfg
        Data data
        File outputDir
        File snippetDir
        String dataJson
        String configJson
        String headHtml
        String descriptionHtml
        String selfContainedJavaScript
    }
}
