# Technology Registry
Tool for building visualizations of Technology Registry. To understand what is Technology Registry and what may be output
of this tool, see [my public Technology Registry](http://zdenda.online/registry.html). 
I used this tool in my former job at [Quadient](https://www.quadient.com/) where we built our company's Technology Registry.

Currently, the main visualization is HTML/JS which was inspired by the code of 
[Zalando Technology Radar](https://github.com/zalando/tech-radar).
This HTML visualization was improved in some ways. This is the list of main changes:
- Way more bigger customization of final output
- Separated data from its visualization and changed data structure
- Changed "rings" (categories) which has inverted direction (from middle to sides) opposed to known radars
- Unique colors for every lifecycle-category combination, mainly for easier recognition of lifecycle phase
- Code clean-up

## Prerequisites
- Java 6 or higher

## Configuration
The tool accepts one parameter which is a path to YAML configuration file. The sample with comments that describes 
all parameters can be found in `sample/config.yml`. The configuration file contains two main sections `data` and `output`.

#### Data
The data for final Technology Registry can be provided from various sources. The parameter `provider` specifies from 
where the data should be taken. There are two built-in providers:
- `json-file`: Simple JSON file (see `sample/data.json`)
- `confluence-zip`: ZIP archive containing exported Confluence space as XML 
  (in Confluence web application: Space Settings - Content Tools)
  
If custom provider is needed, simply implement `DataProvider` interface and register it in `BuildRegistry.DATA_PROVIDERS`.

#### Output
The output is transformation of data to final form (typically some kind of visualization). The parameter `builder`
specifies what will build the output. There is single built-in builder:
- `html`: Builds 2 HTML outputs as there are two typical HTML use-cases:
  - Single self-contained HTML file for easy distribution 
  => use `<output.dir>/registry.html` file
  - Embeddable HTML snippet (embed registry in the existing web-page)
  => use contents of `<output.dir>/snippet` directory (see `<output.dir>/snippet/readme.txt`)
  
If custom builder is needed, simply implement `OutputBuilder` interface and register it in `BuildRegistry.OUTPUT_BUILDERS`.

## Running
There are two options how to run the application.

1. Run tool directly from the sources (with Gradle application plugin)
2. Build distributable package (ZIP or TAR) and then run tool from built scripts

#### Run From Sources
To run the tool directly, run Gradle wrapper task `buildRegistry` add pass path to configuration file as `-Pconfig=<pathToFile>`:
```
./gradlew buildRegistry -Pconfig="./sample/config.yml"
```
** For Windows, use `.\gradlew.bat`

#### Distributable Package
To build a distributable package, run Gradle wrapper with task `distZip` or `distTar` 
(packages will be built into `build/distributions` directory):
```
./gradlew distZip
./gradlew distTar
```
** For Windows, use `.\gradlew.bat`

The built archive contains generated shell and cmd scripts for running the tool (in the `bin` directory). 
To run the tool with these scripts, pass path to configuration as parameter without any prefix:
```
bin/technology-registry "./sample/config.yml"
```
** For Windows, use `technology-registry.bat`


## License
This tool is published under MIT license which basically means "do everything you want with it but without any
warranty". I would be grateful if you would include the link to this repository but it is not necessary :-)
The full text of license can be found in `LICENSE` file.

## Special Thanks
- J. Vondrous that helped me to parse exported Confluence space (entities.xml)
- Anyone who uses this tool and keeps the link to this repo :-)
